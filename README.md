2PTennis - Using spritekit.framework + multipeerconnectivity.framework
======================================================================

2PTennis is a 2D game developed for iOS platforms using spritekit framework and multipeerConnectivity framework. 
The app allows the user to play the game in an multiplayer environment. The players communicate using bluetooth+wifi.
There is no internet connectivity required. The app allows the users to send game request, and if the other player accepts the request, the game will be started. The ball direction,velocity, impulse and a few parameters are transfered to the other device with out any change. So that the two screen acts as two sides of a table.

Compatability : iOS7+


![alt tag](https://drive.google.com/uc?export=download&id=0B8jSQxugFfdyQmxLNk0yQlI2ZFk)
