//
//  MyScene.m
//  BallGame
//
//  Created by Jeethu on 07/08/14.
//  Copyright (c) 2014 JKT. All rights reserved.
//

#import "MyScene.h"
#import "AppDelegate.h"
static const uint32_t ballCategory     =  0x1 << 0;
static const uint32_t batCategory     =  0x1 << 1;
static const uint32_t borderCategory     =  0x1 << 2;
static const uint32_t borderCategoryNull     =  0x1 << 3;
static const uint32_t borderCategoryGameOver     =  0x1 << 4;
@implementation MyScene
@synthesize ball,bat;
bool isHit;
bool hi;
- (void)handleReceivedDataWithNotification:(NSNotification *)notification {
    
    [self.gameover removeFromParent];
    isHit=false;
//    NSLog(@"Message %f",[[notification.userInfo objectForKey:@"positionX"] floatValue]);
    ball.position = CGPointMake(self.frame.size.width-[[notification.userInfo objectForKey:@"positionX"] floatValue],self.size.height);
//    [self addChild:ball];
    
    [ball.physicsBody setVelocity:CGVectorMake(0,0)];
    [ball.physicsBody setVelocity:CGVectorMake(-[[notification.userInfo objectForKey:@"velocityX"] floatValue], -   [[notification.userInfo objectForKey:@"velocityY"] floatValue])];
}

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleReceivedDataWithNotification:)
                                                     name:@"Pilot_DidReceiveDataNotification"
                                                   object:nil];

        self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        isHit=false;
        
        
                self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        self.physicsWorld.gravity = CGVectorMake(0.0f, 0.0f);
        self.physicsWorld.contactDelegate = self;
        
        [self addBackground];
        [self InitializeBall];
        [self addChild:ball];
        
//        [self InitializeBat];
//        [self addChild:bat];
        
        [self InitializeBoarder];
        [self InitializeRestart];
        [self initializeGameover];
        [self initializeScore];
        [self InitializeBack];
        
    }
    return self;
}
-(void)addBackground
{
    SKSpriteNode* background = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"Table"] size:self.size];
    background.position = CGPointMake(self.size.width/2,self.size.height/2);
    [self addChild:background];
}
-(void)InitializeRestart
{
    SKSpriteNode* border = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"restart"] size:CGSizeMake(30,30)];
    border.position = CGPointMake(30,self.size.height-50);
    border.name = @"restart";
    [self addChild:border];
    
}

-(void)InitializeBack
{
    SKSpriteNode* border = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"back"] size:CGSizeMake(30,30)];
    border.position = CGPointMake(self.size.width-30,self.size.height-50);
    border.name = @"back";
    [self addChild:border];
    
}
-(void)InitializeBoarder
{
    SKSpriteNode* border = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"top"] size:CGSizeMake(self.frame.size.width,10)];
    //    tPlayer.position = CGPointMake(self.frame.size.width/4,self.frame.size.height/2);
    border.position = CGPointMake(self.frame.size.width/2,self.size.height+10);
    border.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:border.frame.size];
    border.physicsBody.categoryBitMask = borderCategoryNull;
    border.physicsBody.contactTestBitMask = batCategory | ballCategory;
    border.physicsBody.collisionBitMask = batCategory;
    border.physicsBody.usesPreciseCollisionDetection = YES;
    border.zPosition=1;
    border.name=@"borderTop";
    border.physicsBody.allowsRotation = NO;
    border.physicsBody.dynamic = NO;
    
    [self addChild:border];
    
    
    SKSpriteNode* border1 =[border copy];
    border1.texture = [SKTexture textureWithImageNamed:@"bottom"];
     border1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:border1.frame.size];
    border1.physicsBody.allowsRotation = NO;
    border1.physicsBody.dynamic = NO;
    border1.physicsBody.categoryBitMask = borderCategoryGameOver;
    border1.physicsBody.contactTestBitMask = batCategory | ballCategory;
    border1.physicsBody.collisionBitMask = batCategory | ballCategory;

     border1.position = CGPointMake(self.frame.size.width/2,0);
    [self addChild:border1];
    
    SKSpriteNode* border2 =[border1 copy];
    border2.texture = [SKTexture textureWithImageNamed:@"side"];
    border2.size =CGSizeMake(10,self.frame.size.height);
    border2.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:border2.frame.size];
    border2.position = CGPointMake(0,self.frame.size.height/2);
    border2.physicsBody.allowsRotation = NO;
    border2.physicsBody.dynamic = NO;
    border2.physicsBody.categoryBitMask = borderCategory;
    border2.physicsBody.contactTestBitMask = batCategory | ballCategory;
    border2.physicsBody.collisionBitMask = batCategory | ballCategory;
//    border2.physicsBody.categoryBitMask = borderCategory;
    [self addChild:border2];
    
     SKSpriteNode* border3 =[border2 copy];
//    border3.physicsBody.categoryBitMask = borderCategory;
    border3.physicsBody.categoryBitMask = borderCategory;
    border3.physicsBody.contactTestBitMask = batCategory | ballCategory;
    border3.physicsBody.collisionBitMask = batCategory | ballCategory;
    border3.position = CGPointMake(self.frame.size.width,self.frame.size.height/2);
    [self addChild:border3];
}

-(void)InitializeBall
{
    ball = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"ball"] size:CGSizeMake(50,50)];
    //    tPlayer.position = CGPointMake(self.frame.size.width/4,self.frame.size.height/2);
    ball.position = CGPointMake(self.frame.size.width/4,self.size.height/2);
    ball.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:25];
    ball.physicsBody.categoryBitMask = ballCategory;
    ball.physicsBody.contactTestBitMask = batCategory | borderCategory |borderCategoryGameOver;
    ball.physicsBody.collisionBitMask = batCategory | borderCategory | borderCategoryGameOver;
    ball.physicsBody.usesPreciseCollisionDetection = YES;
    ball.zPosition=2;
    ball.name=@"ball";
    ball.physicsBody.dynamic = YES;
//    ball.physicsBody.mass = 100;
    ball.physicsBody.linearDamping = 0.3f;
//    ball.physicsBody.friction = 1.0f;
    ball.physicsBody.restitution=1;
  
}

-(void)InitializeBat
{
    bat = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:@"bat"] size:CGSizeMake(60,80)];
    //    tPlayer.position = CGPointMake(self.frame.size.width/4,self.frame.size.height/2);
    bat.position = CGPointMake(self.frame.size.width/2,self.size.height/4);
    bat.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(80,50)];
    bat.physicsBody.categoryBitMask = batCategory;
    bat.physicsBody.contactTestBitMask = ballCategory;
    bat.physicsBody.collisionBitMask = batCategory;
    bat.physicsBody.usesPreciseCollisionDetection = YES;
    bat.zPosition=1;
    bat.name=@"bat";
    bat.physicsBody.mass = 100;
    
    bat.physicsBody.dynamic = NO;
    bat.physicsBody.allowsRotation = NO;
    //
}
- (void)didBeginContact:(SKPhysicsContact *)contact
{
    SKPhysicsBody *firstBody, *secondBody;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    
    if ((firstBody.categoryBitMask == ballCategory) && (secondBody.categoryBitMask == batCategory) )
    {
//        NSLog(@"Touch");
//        ball.physicsBody.velocity = CGVectorMake(0,100);
        isHit = true;
        
//        [ball.physicsBody setVelocity:CGVectorMake(0,0)];
        if(!hi)
        {
            hi=true;
//            isHit=true;
        }
//        [ball.physicsBody applyImpulse:CGVectorMake(0,10)];
        
        
        
//        NSLog(@"point %f",[bat convertPoint:contact.contactPoint fromNode:ball].y);
        
        
        
//        [ball.physicsBody applyImpulse:CGVectorMake((contact.contactPoint.x - bat.position.x)/5, 100 * ball.physicsBody.mass)];
         [ball.physicsBody applyImpulse:CGVectorMake((contact.contactPoint.x - bat.position.x)/10, 100 * ball.physicsBody.mass)];
        
//        ball.physicsBody.angularDamping=0.0;
        
//        NSLog(@"imp     %f",contact.collisionImpulse);
//        }
        
        
//        CGPoint pointIn=
        
        
        
//        NSLog(@"poi     %f",contact.contactPoint.x - bat.position.x);
//        [ball.physicsBody applyForce:CGVectorMake(0,100)];
    }
    else if ((firstBody.categoryBitMask == ballCategory) && (secondBody.categoryBitMask == borderCategoryNull) )
    {
        
        
        if(isHit)
        {
//        NSLog(@"ball detailsx %f",ball.position.x);
//        NSLog(@"ball details y%f",ball.position.y);
        //        [ball removeFromParent];
        NSMutableArray *myArr1 =[[NSMutableArray alloc]initWithObjects:@"cross",[NSString stringWithFormat:@"%f",ball.physicsBody.velocity.dx],[NSString stringWithFormat:@"%f",ball.physicsBody.velocity.dy],[NSString stringWithFormat:@"%f",ball.position.x],[NSString stringWithFormat:@"%f",ball.position.y],nil];
        NSData* myData1 = [NSKeyedArchiver archivedDataWithRootObject:myArr1];
        [self.appDelegate.mpcHandler.session sendData:myData1 toPeers:[self.appDelegate.mpcHandler.session connectedPeers] withMode:MCSessionSendDataReliable error:nil];
        
        
//        ball.position = CGPointMake(10000,10000);
        //    [self addChild:ball];
//        [ball.physicsBody setVelocity:CGVectorMake(0,0)];
        }

    }
    else if ((firstBody.categoryBitMask == ballCategory) && (secondBody.categoryBitMask == borderCategory) )
    {
        if(!hi)
        {
            hi=true;
        }
        isHit=true;
    }
    else if ((firstBody.categoryBitMask == ballCategory) && (secondBody.categoryBitMask == borderCategoryGameOver) )
    {
        ball.physicsBody.angularVelocity = 0;
        ball.physicsBody.velocity = CGVectorMake(0, 0);
        [self addChild:self.gameover];
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
//    NSLog(@"Touches began");
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    if([node.name isEqualToString:@"bat"])
    {
        _selectedNode = node;
    }
    
    else if([node.name isEqualToString:@"restart"])
    {
        [self.gameover removeFromParent];
        ball.position = CGPointMake(self.frame.size.width/4,self.size.height/2);
        ball.physicsBody.velocity = CGVectorMake(0, 0);
    }
    else if([node.name isEqualToString:@"ball"])
    {
        if(!hi)
        {
            hi=true;
        }
        isHit=TRUE;
        [ball.physicsBody applyImpulse:CGVectorMake((ball.position.x-location.x)/2, 50)];

    }
    
    
//        CGPoint positionInScene = [touch locationInNode:self];
//        CGPoint previousPosition = [touch previousLocationInNode:self];
//        
//        CGPoint translation = CGPointMake(positionInScene.x - previousPosition.x, positionInScene.y - previousPosition.y);
//          CGPoint position = [node position];
//        
//        [node setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
//    }
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
    }
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
//    NSLog(@"Touches moved");
    UITouch *touch = [touches anyObject];
    
    CGPoint positionInScene = [touch locationInNode:self];
    CGPoint previousPosition = [touch previousLocationInNode:self];
    CGPoint translation = CGPointMake(positionInScene.x - previousPosition.x, positionInScene.y - previousPosition.y);
    CGPoint position = [_selectedNode position];
    if([[_selectedNode name] isEqualToString:@"bat"] && bat.position.y<=self.view.frame.size.height-20) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    else
    {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, self.view.frame.size.height-20)];
    }
     SKNode *node = [self nodeAtPoint:positionInScene];
    if([node.name isEqualToString:@"ball"])
    {
        if(!hi)
        {
            hi=true;
        }
        isHit=true;
        [ball.physicsBody applyImpulse:CGVectorMake((ball.position.x-positionInScene.x)/2, 20)];
        
    }
    }
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    _selectedNode = nil;
}
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    _selectedNode = nil;
}
-(void)initializeGameover
{
    self.gameover = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    self.gameover.text = @"Game Over, You Lose";
    self.gameover.fontSize = 20;
    self.gameover.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    
}
-(void)initializeScore
{
    self.score = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    self.score.text = @"Score : 0";
    self.score.fontSize = 20;
    self.score.position = CGPointMake(self.size.width/3,self.size.height-60);
    [self addChild:_score];
   
}
-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
//    if(ball.position.y+CGRectGetHeight(ball.frame)/2 > self.frame.size.height && isHit)
//    {
//        
//        NSLog(@"ball detailsx %f",ball.position.x);
//        NSLog(@"ball details y%f",ball.position.y);
//        isHit = false;
////        [ball removeFromParent];
//        NSMutableArray *myArr1 =[[NSMutableArray alloc]initWithObjects:@"cross",[NSString stringWithFormat:@"%f",ball.physicsBody.velocity.dx],[NSString stringWithFormat:@"%f",ball.physicsBody.velocity.dy],[NSString stringWithFormat:@"%f",ball.position.x],[NSString stringWithFormat:@"%f",ball.position.y],nil];
//        NSData* myData1 = [NSKeyedArchiver archivedDataWithRootObject:myArr1];
//        [self.appDelegate.mpcHandler.session sendData:myData1 toPeers:[self.appDelegate.mpcHandler.session connectedPeers] withMode:MCSessionSendDataReliable error:nil];
//    }
    
    if(hi)
    {
        if(ball.physicsBody.velocity.dy<=(unsigned int)5)
        {
//            NSLog(@"%f %f",ball.position.y,self.view.frame.size.height*3/4);
        if(ball.position.y >= self.view.frame.size.height*3/4)
        {
        }
        }
    }
}


@end
