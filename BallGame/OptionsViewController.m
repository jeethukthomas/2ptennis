//
//  OptionsViewController.m
//  MPCDemo
//
//  Created by Gabriel Theodoropoulos on 21/02/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import "OptionsViewController.h"

#import "AppDelegate.h"

@interface OptionsViewController ()

@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIButton *pairBtn;
@property (strong, nonatomic) IBOutlet UIButton *aboutBtn;
@property (strong, nonatomic) IBOutlet UILabel *bottomLabel;

@end

@implementation OptionsViewController

#pragma mark -
#pragma mark View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.appDelegate.mpcHandler setupPeerWithDisplayName:[NSString stringWithFormat:@"Pilot - %@",[UIDevice currentDevice].name]];
    [self.appDelegate.mpcHandler setupSession];
    [self.appDelegate.mpcHandler advertiseSelf:true];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(peerChangedStateWithNotification:)
                                                 name:@"MPCDemo_DidChangeStateNotification"
                                               object:nil];
    
    [self.txtPlayerName setDelegate:self];
    
    [_pairBtn.layer setCornerRadius:CGRectGetHeight(_pairBtn.frame)/2];
    [_pairBtn.layer setBorderWidth:2];
    [_pairBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [_aboutBtn.layer setCornerRadius:CGRectGetHeight(_aboutBtn.frame)/2];
    [_aboutBtn.layer setBorderWidth:2];
    [_aboutBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [_bottomLabel.layer setCornerRadius:10];
}

#pragma mark -
#pragma mark Browser View Controller Delegate Methods
- (void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController {
    [self.appDelegate.mpcHandler.browser dismissViewControllerAnimated:YES completion:nil];
}

- (void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController {
    [self.appDelegate.mpcHandler.browser dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark Text Field Delegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.txtPlayerName resignFirstResponder];
    
    if (self.appDelegate.mpcHandler.peerID != nil) {
        [self.appDelegate.mpcHandler.session disconnect];
        
        self.appDelegate.mpcHandler.peerID = nil;
        self.appDelegate.mpcHandler.session = nil;
    }
    
    [self.appDelegate.mpcHandler setupPeerWithDisplayName:self.txtPlayerName.text];
    [self.appDelegate.mpcHandler setupSession];
    [self.appDelegate.mpcHandler advertiseSelf:self.swVisible.isOn];
    
    return YES;
}

#pragma mark -
#pragma mark Actions
- (IBAction)disconnect:(id)sender {
    [self.appDelegate.mpcHandler.session disconnect];
}

- (IBAction)searchForPlayers:(id)sender {
    if (self.appDelegate.mpcHandler.session != nil) {
        [[self.appDelegate mpcHandler] setupBrowser];
        [[[self.appDelegate mpcHandler] browser] setDelegate:self];
        
        [self presentViewController:self.appDelegate.mpcHandler.browser
                           animated:YES
                         completion:nil];
    }
}

- (IBAction)toggleVisibility:(id)sender {
    [self.appDelegate.mpcHandler advertiseSelf:self.swVisible.isOn];
}

#pragma mark -
#pragma mark Notification Handling
- (void)peerChangedStateWithNotification:(NSNotification *)notification {
    // Get the state of the peer.
    int state = [[[notification userInfo] objectForKey:@"state"] intValue];
    
    // We care only for the Connected and the Not Connected states.
    // The Connecting state will be simply ignored.
    if (state != MCSessionStateConnecting) {
        // We'll just display all the connected peers (players) to the text view.
        NSString *allPlayers = @"Other players connected with:\n\n";
        
        for (int i = 0; i < self.appDelegate.mpcHandler.session.connectedPeers.count; i++) {
            NSString *displayName = [[self.appDelegate.mpcHandler.session.connectedPeers objectAtIndex:i] displayName];
            
            allPlayers = [allPlayers stringByAppendingString:@"\n"];
            allPlayers = [allPlayers stringByAppendingString:displayName];
        }
        
        [self.tvPlayerList setText:allPlayers];
    }
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"homeToScene"] )
    {
        if(![self.appDelegate.mpcHandler.session.connectedPeers count]>0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No pair detected"
                                                            message:@"Since 2P-Tennis is a multiplayer game, you must establish a connection with your co-player before you starts playing "
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return NO;

        }
    }
    
    return YES;
}

@end
