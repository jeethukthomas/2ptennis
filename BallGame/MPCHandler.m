//
//  MPCHandler.m
//  MPCDemo
//
//  Created by Gabriel Theodoropoulos on 21/02/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import "MPCHandler.h"
#import "AppDelegate.h"
@implementation MPCHandler

#pragma mark -
#pragma mark Public Methods
- (void)setupPeerWithDisplayName:(NSString *)displayName {
    self.peerID = [[MCPeerID alloc] initWithDisplayName:displayName];
}

- (void)setupSession {
    self.session = [[MCSession alloc] initWithPeer:self.peerID];
    self.session.delegate = self;
}

- (void)setupBrowser {
    self.browser = [[MCBrowserViewController alloc] initWithServiceType:@"my-game" session:_session];
}

- (void)advertiseSelf:(BOOL)advertise {
    if (advertise) {
        self.advertiser = [[MCAdvertiserAssistant alloc] initWithServiceType:@"my-game" discoveryInfo:nil session:self.session];
        [self.advertiser start];
        
    } else {
        [self.advertiser stop];
        self.advertiser = nil;
    }
}

#pragma mark -
#pragma mark Session Delegate Methods
- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state {
    NSDictionary *userInfo = @{ @"peerID": peerID,
                                @"state" : @(state) };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MPCDemo_DidChangeStateNotification"
                                                            object:nil
                                                          userInfo:userInfo];
    });
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID {
    NSArray* gotArr = (NSArray*)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSDictionary *userInfo = @{ @"message": [gotArr objectAtIndex:0],
                                                                @"peerID": peerID,
                                                                @"velocityX": [gotArr objectAtIndex:1],
                                                                @"velocityY": [gotArr objectAtIndex:2],
                                @"positionX": [gotArr objectAtIndex:3],
                                @"positionY": [gotArr objectAtIndex:4]
                                };
                                
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Pilot_DidReceiveDataNotification"
                                                            object:nil
                                                          userInfo:userInfo];
    });
    
//    NSArray* gotArr = (NSArray*)[NSKeyedUnarchiver unarchiveObjectWithData:data];
//    NSLog(@"Message %@",[gotArr objectAtIndex:0]);
//    if([[gotArr objectAtIndex:0] isEqualToString:@""])
//        {
//    NSDictionary *userInfo = @{ @"message": [gotArr objectAtIndex:0],
//                                @"peerID": peerID,
//                                @"video": [gotArr objectAtIndex:1]};
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"Passenger_DidReceiveDataNotification"
//                                                            object:nil
//                                                          userInfo:userInfo];
//    });
//        }
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress {
    
}

- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error {
    
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID {
    
}

@end
