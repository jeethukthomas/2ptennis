//
//  AppDelegate.h
//  BallGame
//
//  Created by Jeethu on 07/08/14.
//  Copyright (c) 2014 JKT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPCHandler.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) MPCHandler *mpcHandler;
@end
