//
//  OptionsViewController.h
//  MPCDemo
//
//  Created by Gabriel Theodoropoulos on 21/02/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@interface OptionsViewController : UIViewController <MCBrowserViewControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtPlayerName;
@property (weak, nonatomic) IBOutlet UISwitch *swVisible;
@property (weak, nonatomic) IBOutlet UITextView *tvPlayerList;

- (IBAction)disconnect:(id)sender;
- (IBAction)searchForPlayers:(id)sender;
- (IBAction)toggleVisibility:(id)sender;

@end
