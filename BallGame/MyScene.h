//
//  MyScene.h
//  BallGame
//

//  Copyright (c) 2014 JKT. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "AppDelegate.h"
#import <MultipeerConnectivity/MultipeerConnectivity.h>
@interface MyScene : SKScene<SKPhysicsContactDelegate>

@property (nonatomic) SKSpriteNode * ball;
@property (nonatomic) SKSpriteNode * bat;
@property (nonatomic) SKNode * selectedNode;
@property (nonatomic, strong) SKLabelNode *gameover;
@property (nonatomic, strong) SKLabelNode *score;
@property (nonatomic, strong) AppDelegate *appDelegate;
@end
